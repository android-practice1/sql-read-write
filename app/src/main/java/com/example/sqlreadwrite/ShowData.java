package com.example.sqlreadwrite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ShowData extends AppCompatActivity {

    ListView listView;
    String[] data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view);

        //TextView textView = findViewById(R.id.show_data);
        listView = (ListView) findViewById(R.id.list_view_layout);
        DataBaseMethod dataBaseMethod = new DataBaseMethod(getApplicationContext());

        // string of database
        data = dataBaseMethod.birthday_data();


        // layout = activity_show_data
        // textview = show_data
        listView.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.activity_show_data, R.id.show_data, data));


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(), UpdateDatabase.class);
                i.putExtra("MyKEY", position); // what the fuck is this?
                startActivity(i);
            }
        });


        /*
        // don't know whats happens here
        String[] data = dataBaseMethod.birthday_data(); // birthday_data if from DataBaseMethod
        String s = "";
        for (int i=0; i < data.length; i++) {
            s = s+ data[i] + "\n\n";
        }

        // textView.setText(data[0]);
        textView.setText(s);
         */
    }
}