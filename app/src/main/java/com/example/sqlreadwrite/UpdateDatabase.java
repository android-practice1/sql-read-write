package com.example.sqlreadwrite;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class UpdateDatabase extends AppCompatActivity {

    EditText editText;
    Button button, button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_database);

        editText = (EditText) findViewById(R.id.edit_text);
        button = (Button) findViewById(R.id.update_database);
        button2 = (Button) findViewById(R.id.delete_database);

        // no idea what the key is
        final int recent_position = getIntent().getIntExtra("MyKEY", 999);

        // object
        final DataBaseMethod obj = new DataBaseMethod(getApplicationContext());

        // String t = obj.fetch_day(recent_position + 1); // 1 = database position
        // e.setText(t);

        // alternative method
        editText.setText(obj.fetch_day(recent_position + 1));

        // move cursor to left
        editText.setSelection(editText.getText().length());

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obj.update_birthday((recent_position+1), editText.getText().toString());
                Toast.makeText(UpdateDatabase.this, "Updated", Toast.LENGTH_SHORT).show();
            }
        });

        // delete button
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obj.delete_bday(obj.fetch_day(recent_position+1));
                Toast.makeText(getApplicationContext(), "Deleted Successfully!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}