package com.example.sqlreadwrite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseMethod extends SQLiteOpenHelper {

    // database verabiale
    private static final String DATABASE_NAME = "birthday_database"; // name of our database
    private static final String TABLE_NAME = "birthday_table"; // name of out table

    private static final String TAB_ID = "id"; // database id 1 2 3 ...
    private static final String TAB_NAME = "name"; // name filed of database
    private static final String TAB_DAYS = "days"; // days field of database

    // constructor
    DataBaseMethod (Context context) {
        // null = cursor factory
        super (context, DATABASE_NAME, null, 1);
    }

    // implemented method of SQLiteOpenHelper class
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        // database query
        // creating a table
        String string = "CREATE TABLE " + TABLE_NAME + "("+TAB_ID+"  INTEGER PRIMARY KEY, "+TAB_NAME+" TEXT, "+TAB_DAYS+" TEXT)";
        sqLiteDatabase.execSQL(string);

    }

    // implemented method of SQLiteOpenHelper class
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // null
    }

    // adding data to database
    // GetterSetter is our getter setter method
    void  addDataOnTable(GetterSetter getterSetter) {
        // sqLiteDatabase is a object
        // Writing Database
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(TAB_NAME, getterSetter.getName()); // implemented from getter setter
        contentValues.put(TAB_DAYS, getterSetter.getDay()); //  implemented from getter setter

        sqLiteDatabase.insert(TABLE_NAME, null, contentValues);
        sqLiteDatabase.close();
    }

    // showing data
    String[] birthday_data() {
        // read
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();

        // database query - reading data
        String q = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = sqLiteDatabase.rawQuery(q, null);

        // i don't understand what was going here
        String[] receive_data = new String[cursor.getCount()]; // getcount - count your data no need to set size
        cursor.moveToFirst();
        if(cursor.moveToFirst()){
            int counter = 0;
            do {
                receive_data[counter] = cursor.getString(cursor.getColumnIndex(TAB_NAME + "")) + "\nBirthday: " + cursor.getString(cursor.getColumnIndex(TAB_DAYS + ""));;
                counter = counter+1;
            } while (cursor.moveToNext());
        }

        return receive_data;

    }

    // locating date
    // locating birthday
    String fetch_day(int id) {

        // read
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();

        // query
        //         table         date                name                      id   = =     id
        String q = "SELECT " + TAB_DAYS + " FROM " + TABLE_NAME + " WHERE " + TAB_ID + " = " +id;
        Cursor cursor = sqLiteDatabase.rawQuery(q, null);

        String s = "";
        cursor.moveToFirst();
        if (cursor.moveToFirst()) {
            s = cursor.getString(cursor.getColumnIndex(TAB_DAYS + ""));
        }

        return s;
    }

    // updating birthday
    int update_birthday(int id, String bday) {

        SQLiteDatabase sq = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TAB_DAYS, bday);

        // query
        return sq.update(TABLE_NAME, cv, TAB_ID + " = ? ", new String[] {id+""});
    }

    // delete birthday
    int delete_bday(String bday){
        SQLiteDatabase s = this.getWritableDatabase();
        return s.delete(TABLE_NAME, TAB_DAYS+" = ?", new String[] {bday});
    }

}
