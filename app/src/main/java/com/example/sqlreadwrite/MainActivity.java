package com.example.sqlreadwrite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText name_EditText = findViewById(R.id.name);
        final EditText date_EditText = findViewById(R.id.date);

        Button save_button = findViewById(R.id.save);
        Button show_button = findViewById(R.id.show);

        // creating object of DatabaseMethod
        final DataBaseMethod dataBaseMethod = new DataBaseMethod(getApplicationContext());

        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // getting text
                String _name = name_EditText.getText().toString();
                String _day = date_EditText.getText().toString();

                // creating object of getter setter class
                // adding to table
                GetterSetter getterSetter = new GetterSetter(_name, _day); // _name = n and _day = d from getter setter
                dataBaseMethod.addDataOnTable(getterSetter);

                // added a toast
                Toast.makeText(getApplicationContext(),"Data Added Successfully", Toast.LENGTH_LONG).show();
            }
        });

        show_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // going for a new activity
                startActivity(new Intent(getApplicationContext(), ShowData.class));
            }
        });

    }
}